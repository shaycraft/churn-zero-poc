import {ChangeDetectorRef, Component} from '@angular/core';
import { User } from './models/user.model';
import { Message } from './models/message.model';

declare const ChurnZero: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public readonly APP_KEY = '1!QKJQj3kL23yOL8kXKSFvSkSFSw12KT6aRATRj6d2nGItB73';
  public title = 'Churn Zero POC';
  public user: User | null = null;
  public messages: Message[] = [];

  constructor(private readonly _cdr: ChangeDetectorRef) {}

  public login(): void {
    this.user = new User('John Doe');
    this.addMessage('user logged in');

    ChurnZero.push(['setAppKey', this.APP_KEY]);
    ChurnZero.push(['setContact', 'sam@servicecore.com', 'xxx123456']);
    // ChurnZero.push(['setModule', 'Demo POC']);

    ChurnZero.push([
      'ChurnZeroEvent:on',
      'connected',
      (data: any) => {
        console.debug(data);
        this.addMessage(JSON.stringify(data));
        this._cdr.detectChanges();
      },
    ]);

    ChurnZero.push(['trackEvent', 'Login']);
  }

  public logout(): void {
    this.user = null;
    ChurnZero.push(['trackEvent', 'Logout']);
    ChurnZero.push(['close']);

    this.addMessage('User logged out');
  }

  public toggleDebug = () => ChurnZero.debug();

  private addMessage(message: string): void {
    this.messages.push({ content: message, timestamp: new Date() });
  }
}
