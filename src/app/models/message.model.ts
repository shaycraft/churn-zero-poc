interface MessageInterface {
  content: string;
  timestamp: Date;
}

export class Message implements  MessageInterface {
  public content: string;
  public timestamp: Date;

  constructor({content, timestamp}: MessageInterface) {
    this.content = content;
    this.timestamp = timestamp ;
  }
}
